# Visualizing system statistics and infrastucture on CLOUDIFIN

A set of services that run as a self-contained web application, developed with `Flask`, which aim at showing system information of an underlying Openstack cluster.

## Services

1. Topology view
2. Histogram view
3. Multi-graph system stats view

## Development

Flask Python web app running on a local machine at DFCTI. 
